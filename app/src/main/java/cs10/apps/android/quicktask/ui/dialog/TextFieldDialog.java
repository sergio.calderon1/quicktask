package cs10.apps.android.quicktask.ui.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.text.InputType;
import android.view.Gravity;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

public class TextFieldDialog extends DialogFragment {
    private TextFieldCallback callback;
    private EditText editText;

    public void setCallback(TextFieldCallback callback) {
        this.callback = callback;
    }

    @NonNull @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        editText = new EditText(getContext());
        editText.setInputType(InputType.TYPE_CLASS_TEXT);
        editText.setGravity(Gravity.CENTER);
        builder.setTitle("Task name");
        builder.setView(editText);
        builder.setPositiveButton("Next", (dialog, which) -> getData());
        builder.setNeutralButton("Cancel", ((dialog, which) -> dismiss()));
        return builder.create();
    }

    private void getData() {
        if (callback != null) callback.readTextField(editText.getText().toString());
    }
}
