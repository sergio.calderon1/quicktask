package cs10.apps.android.quicktask.ui.adapter;

import cs10.apps.android.quicktask.model.Task;

public interface OnDeleteTaskCallback {
    void readDeleted(Task task);
}
