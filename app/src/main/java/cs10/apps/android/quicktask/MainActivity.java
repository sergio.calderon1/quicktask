package cs10.apps.android.quicktask;

import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentContainerView;
import androidx.fragment.app.FragmentManager;
import androidx.room.Room;

import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;

import java.util.Collections;
import java.util.List;

import cs10.apps.android.quicktask.db.TaskDatabase;
import cs10.apps.android.quicktask.ui.adapter.OnDeleteTaskCallback;
import cs10.apps.android.quicktask.ui.dialog.DatePickerCallback;
import cs10.apps.android.quicktask.ui.dialog.DatePickerFragment;
import cs10.apps.android.quicktask.ui.dialog.TextFieldCallback;
import cs10.apps.android.quicktask.ui.dialog.TextFieldDialog;
import cs10.apps.android.quicktask.ui.dialog.TimePickerCallback;
import cs10.apps.android.quicktask.ui.dialog.TimePickerFragment;
import cs10.apps.android.quicktask.helpers.TaskCreationHelper;
import cs10.apps.android.quicktask.model.Task;
import cs10.apps.android.quicktask.ui.fragments.FirstFragment;

public class MainActivity extends AppCompatActivity implements TextFieldCallback,
        DatePickerCallback, TimePickerCallback, OnDeleteTaskCallback {

    private TaskDatabase database;
    private List<Task> sortedTaskList;
    private final TaskCreationHelper creationHelper = new TaskCreationHelper();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        new Thread(this::initiateDb).start();

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(view -> openNewTaskDialog());

        // do not turn off screen
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    private void initiateDb() {
        database = Room.databaseBuilder(getApplicationContext(),
                TaskDatabase.class, "database-tasks").build();

        TaskDatabase database = getDatabase();
        if (database != null){
            sortedTaskList = database.taskDao().getAll();
            Collections.sort(sortedTaskList);
            Collections.reverse(sortedTaskList);
        }
    }

    @Nullable
    public TaskDatabase getDatabase() {
        return database;
    }

    @Nullable
    public List<Task> getSortedTaskList() {
        return sortedTaskList;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void openNewTaskDialog(){
        TextFieldDialog dialog = new TextFieldDialog();
        dialog.setCallback(this);
        dialog.show(getSupportFragmentManager(), "text-field");
    }

    @Override
    public void readTextField(String text) {
        creationHelper.setName(text);
        openDatePickerDialog();
    }

    private void openDatePickerDialog() {
        DatePickerFragment fragment = new DatePickerFragment();
        fragment.setCallback(this);
        fragment.show(getSupportFragmentManager(), "date-picker");
    }

    @Override
    public void readDate(int year, int month, int day) {
        creationHelper.setDate(year, month, day);
        openTimePickerDialog();
    }

    private void openTimePickerDialog() {
        TimePickerFragment fragment = new TimePickerFragment();
        fragment.setCallback(this);
        fragment.show(getSupportFragmentManager(), "time-picker");
    }

    @Override
    public void readTime(int hour, int minute) {
        creationHelper.setTime(hour, minute);
        TaskDatabase database = getDatabase();
        finishCreation(database);
    }

    private void finishCreation(TaskDatabase database) {
        Task task = creationHelper.getTask();
        if (database != null) new Thread(() -> database.taskDao().insert(task)).start();
        sortedTaskList.add(task);
        Collections.sort(sortedTaskList);
        Collections.reverse(sortedTaskList);
        forceAdapterUpdate();
        creationHelper.clear();
    }

    private void forceAdapterUpdate() {
        Fragment f = getForegroundFragment();
        if (f instanceof FirstFragment){
            FirstFragment ff = (FirstFragment) f;
            ff.updateAdapter();
        }
    }

    public Fragment getForegroundFragment(){
        Fragment navHostFragment = getSupportFragmentManager().findFragmentById(R.id.nav_host_fragment);
        return navHostFragment == null ? null : navHostFragment.getChildFragmentManager().getFragments().get(0);
    }

    @Override
    public void readDeleted(Task task) {
        new Thread(() -> database.taskDao().delete(task)).start();
    }
}