package cs10.apps.android.quicktask.ui.dialog;

public interface TimePickerCallback {
    void readTime(int hour, int minute);
}
