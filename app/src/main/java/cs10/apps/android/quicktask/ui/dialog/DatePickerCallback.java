package cs10.apps.android.quicktask.ui.dialog;

public interface DatePickerCallback {
    void readDate(int year, int month, int day);
}
