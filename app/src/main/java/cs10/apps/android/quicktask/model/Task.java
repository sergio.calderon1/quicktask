package cs10.apps.android.quicktask.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.util.Objects;

@Entity
public class Task implements Comparable<Task> {

    @PrimaryKey @ColumnInfo(name = "start")
    private long creationTimestamp;

    @ColumnInfo(name = "end")
    private long endTimestamp;

    private String name;

    @Ignore
    private String originalPercentageValue;

    /** custom methods **/
    public final long getDiffCreationTS(){
        return System.currentTimeMillis() - creationTimestamp;
    }

    public final long getDiffEndTS(){
        return endTimestamp - System.currentTimeMillis();
    }

    public final long getMaxDiffTS() {
        return endTimestamp - creationTimestamp;
    }

    public final double getPriority(){
        return getDiffCreationTS() * 100d / getDiffEndTS();
    }

    public final double getScore(){
        double small = 1 - Math.pow((double) getDiffCreationTS() / getMaxDiffTS(), 2);
        return small * 100;
    }

    @NonNull
    public final String getPercentage(){
        long value = getDiffCreationTS() * 100 / getMaxDiffTS();
        if (value > 100) return "X";
        if (value > 0) return String.valueOf(value);
        return "";
    }

    /** ui methods **/

    public final boolean shouldMove(){
        String currentValue = getPercentage();

        if (originalPercentageValue == null) {
            originalPercentageValue = currentValue;
            return false;
        }

        if (!originalPercentageValue.equals(currentValue)){
            originalPercentageValue = currentValue;
            return true;
        }

        return false;
    }

    /** default methods **/

    public String getName() {
        return name;
    }

    public long getCreationTimestamp() {
        return creationTimestamp;
    }

    public long getEndTimestamp() {
        return endTimestamp;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCreationTimestamp(long creationTimestamp) {
        this.creationTimestamp = creationTimestamp;
    }

    public void setEndTimestamp(long endTimestamp) {
        this.endTimestamp = endTimestamp;
    }

    @Override
    public int compareTo(Task o) {
        return Double.compare(this.getPriority(), o.getPriority());
    }
}
