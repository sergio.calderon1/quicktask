package cs10.apps.android.quicktask.db;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import cs10.apps.android.quicktask.model.Task;

@Database(entities = {Task.class}, version = 1)
public abstract class TaskDatabase extends RoomDatabase {
    public abstract TaskDao taskDao();
}
