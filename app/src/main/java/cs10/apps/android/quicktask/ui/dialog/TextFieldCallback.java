package cs10.apps.android.quicktask.ui.dialog;

public interface TextFieldCallback {
    void readTextField(String text);
}
