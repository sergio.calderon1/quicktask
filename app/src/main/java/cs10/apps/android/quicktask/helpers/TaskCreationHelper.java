package cs10.apps.android.quicktask.helpers;

import java.util.Calendar;

import cs10.apps.android.quicktask.model.Task;

public class TaskCreationHelper {
    private Task task = new Task();
    private final Calendar calendar = Calendar.getInstance();

    public void clear(){
        task = new Task();
    }

    public void setName(String name) {
        this.task.setName(name);
    }

    public void setDate(int year, int month, int day){
        calendar.set(year, month, day);
    }

    public void setTime(int hour, int minute){
        calendar.set(Calendar.HOUR_OF_DAY, hour);
        calendar.set(Calendar.MINUTE, minute);
        task.setCreationTimestamp(System.currentTimeMillis());
        task.setEndTimestamp(calendar.getTimeInMillis());
    }

    public Task getTask() {
        return task;
    }
}
