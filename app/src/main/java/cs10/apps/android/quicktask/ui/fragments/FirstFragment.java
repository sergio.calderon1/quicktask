package cs10.apps.android.quicktask.ui.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.LinkedList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import cs10.apps.android.quicktask.MainActivity;
import cs10.apps.android.quicktask.R;
import cs10.apps.android.quicktask.model.Task;
import cs10.apps.android.quicktask.ui.adapter.OnDeleteTaskCallback;
import cs10.apps.android.quicktask.ui.adapter.SwipeToDeleteCallback;
import cs10.apps.android.quicktask.ui.adapter.TaskAdapter;

public class FirstFragment extends Fragment {
    private TaskAdapter adapter;
    private RecyclerView rv;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_first, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        view.findViewById(R.id.button_first).setOnClickListener(v -> updateAdapter());
        
        /*view.findViewById(R.id.button_first).setOnClickListener(
                view1 -> NavHostFragment.findNavController(FirstFragment.this)
                    .navigate(R.id.action_FirstFragment_to_SecondFragment));*/

        rv = view.findViewById(R.id.recycler_first);
        adapter = new TaskAdapter(getContext());

        rv.setLayoutManager(new LinearLayoutManager(getContext()));
        rv.setAdapter(adapter);

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new SwipeToDeleteCallback(adapter));
        itemTouchHelper.attachToRecyclerView(rv);

        updateAdapter();
        startAutoUpdateThread();
    }

    private void startAutoUpdateThread() {
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                try {
                    getActivity().runOnUiThread(() -> doAutoMove());
                } catch (Exception e){
                    e.printStackTrace();
                    timer.cancel();
                }
            }
        }, 15000, 60000);
    }

    private void doAutoMove(){
        if (adapter.moveItems()) rv.smoothScrollToPosition(0);
    }

    @Nullable
    private OnDeleteTaskCallback getCallbackActivity(){
        if (getActivity() instanceof OnDeleteTaskCallback){
            return (OnDeleteTaskCallback) getActivity();
        } else return null;
    }

    private List<Task> getTaskList(){
        if (getActivity() instanceof MainActivity){
            MainActivity activity = (MainActivity) getActivity();
            return activity.getSortedTaskList();
        } else return new LinkedList<>();
    }

    public void updateAdapter(){
        //Toast.makeText(getContext(), "Updating...", Toast.LENGTH_SHORT).show();
        adapter.setSortedTaskList(getTaskList());
        adapter.setCallback(getCallbackActivity());
        adapter.notifyDataSetChanged();
    }

}