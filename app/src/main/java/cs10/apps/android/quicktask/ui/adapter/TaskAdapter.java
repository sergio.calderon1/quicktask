package cs10.apps.android.quicktask.ui.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.List;

import cs10.apps.android.quicktask.R;
import cs10.apps.android.quicktask.helpers.FormatUtils;
import cs10.apps.android.quicktask.model.Task;

public class TaskAdapter extends RecyclerView.Adapter<TaskAdapter.TaskHolder>{
    private List<Task> sortedTaskList;
    private final Context context;
    private OnDeleteTaskCallback callback;
    private final DecimalFormat df = new DecimalFormat("#.##");

    public TaskAdapter(Context context) {
        this.context = context;
    }

    public void setCallback(OnDeleteTaskCallback callback) {
        this.callback = callback;
    }

    public void setSortedTaskList(List<Task> sortedTaskList) {
        this.sortedTaskList = sortedTaskList;
    }

    @NonNull
    @Override
    public TaskHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.task_item, parent, false);
        return new TaskHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TaskHolder holder, int position) {
        Task t = sortedTaskList.get(position);
        showCreationInfo(holder, t.getCreationTimestamp());
        showEndInfo(holder, t.getEndTimestamp());

        holder.title.setText(t.getName());
        holder.bigNumber.setText(t.getPercentage());
        holder.detail.setVisibility(View.GONE);

        //holder.detail.setText(context.getString(R.string.task_priority, t.getPriority()));
        //holder.detail.setText(context.getString(R.string.task_score, df.format(t.getScore())));
        holder.card.setCardBackgroundColor(ContextCompat.getColor(context, getColorByPriority(t.getPriority())));
    }

    private void showEndInfo(@NonNull TaskHolder holder, long millis) {
        Calendar end = Calendar.getInstance();
        end.setTimeInMillis(millis);

        holder.description.setText(context.getString(R.string.task_end_description,
                end.get(Calendar.HOUR_OF_DAY),
                FormatUtils.getTwoDigits(end.get(Calendar.MINUTE)),
                FormatUtils.getMonthName(end.get(Calendar.MONTH)),
                end.get(Calendar.DAY_OF_MONTH)));
    }

    private void showCreationInfo(@NonNull TaskHolder holder, long millis) {
        Calendar start = Calendar.getInstance();
        start.setTimeInMillis(millis);

        holder.description_top.setText(context.getString(R.string.task_create_description,
                FormatUtils.getMonthName(start.get(Calendar.MONTH)),
                start.get(Calendar.DAY_OF_MONTH)));
    }

    private int getColorByPriority(double priority){
        if (priority < 0) return android.R.color.black;
        if (priority < 12) return android.R.color.holo_green_dark;
        if (priority < 30) return android.R.color.holo_blue_dark;
        if (priority < 180) return android.R.color.holo_orange_dark;
        return android.R.color.holo_red_dark;
    }

    @Override
    public int getItemCount() {
        return sortedTaskList != null ? sortedTaskList.size() : 0;
    }

    public final boolean moveItems(){
        if (sortedTaskList == null) return false;
        int index = 0;

        for (Task t : sortedTaskList){
            if (t.shouldMove()){
                Log.i("MOVE", "Moving " + t.getName());
                sortedTaskList.remove(index);
                notifyItemRemoved(index);
                sortedTaskList.add(0, t);
                notifyItemInserted(0);
                //notifyItemMoved(index,0);
                //notifyItemChanged(0);
                return true;
            }

            index++;
        }

        return false;
    }

    public void deleteItem(int position) {
        if (callback != null) callback.readDeleted(sortedTaskList.get(position));
        else throw new RuntimeException("Not callback set");

        sortedTaskList.remove(position);
        notifyItemRemoved(position);
    }

    public static class TaskHolder extends RecyclerView.ViewHolder {
        TextView title, description_top, description, detail, bigNumber;
        CardView card;

        public TaskHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.task_title);
            description_top = itemView.findViewById(R.id.task_description_top);
            description = itemView.findViewById(R.id.task_description);
            detail = itemView.findViewById(R.id.task_detail);
            bigNumber = itemView.findViewById(R.id.task_big_number);
            card = itemView.findViewById(R.id.task_card);
        }
    }

}
